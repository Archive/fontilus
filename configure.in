-*- mode: autoconf -*-
AC_PREREQ(2.52)

m4_define(fontilus_version, 0.4)
m4_define(gnomevfs_minver, 2.0.0)
m4_define(fontconfig_minver, 1.0.0)
m4_define(xft_minver, 2.0.0)
m4_define(gtk_minver, 2.0.0)
m4_define(libbonobo_minver, 2.0.0)
m4_define(libgnomeui_minver, 2.1.90)

AC_INIT([fontilus], [fontilus_version])
AC_CONFIG_SRCDIR([src/font-method.c])
AM_CONFIG_HEADER(config.h)

AM_INIT_AUTOMAKE(fontilus, fontilus_version)

AC_PROG_CC
AC_ISC_POSIX
AC_HEADER_STDC

AC_DISABLE_STATIC
AC_PROG_LIBTOOL

dnl get rid of the -export-dynamic stuff from the configure flags ...
export_dynamic=`(./libtool --config; echo eval echo \\$export_dynamic_flag_spec) | sh`

PKG_CHECK_MODULES(FONT_METHOD,
  [gnome-vfs-module-2.0 >= gnomevfs_minver dnl
   fontconfig >= fontconfig_minver])
AC_SUBST(FONT_METHOD_CFLAGS)
AC_SUBST(FONT_METHOD_LIBS)
if test -n "$export_dynamic"; then
  FONT_METHOD_LIBS=`echo $FONT_METHOD_LIBS | sed -e "s/$export_dynamic//"`
fi

PKG_CHECK_MODULES(THUMBNAILER,
  [gnome-vfs-2.0 >= gnomevfs_minver dnl
   gdk-pixbuf-2.0 >= gtk_minver])

AC_PATH_PROG(FREETYPE_CONFIG, freetype-config, no)
if test "x$FREETYPE_CONFIG" = xno; then
    AC_MSG_ERROR([Could not find freetype-config script])
fi
THUMBNAILER_CFLAGS="$THUMBNAILER_CFLAGS `$FREETYPE_CONFIG --cflags`"
THUMBNAILER_LIBS="$THUMBNAILER_LIBS `$FREETYPE_CONFIG --libs`"
if test -n "$export_dynamic"; then
  THUMBNAILER_LIBS=`echo $THUMBNAILER_LIBS | sed -e "s/$export_dynamic//"`
fi

AC_SUBST(THUMBNAILER_CFLAGS)
AC_SUBST(THUMBNAILER_LIBS)

PKG_CHECK_MODULES(FONT_VIEW,
  [gnome-vfs-2.0 >= gnomevfs_minver dnl
   xft >= xft_minver gtk+-2.0 >= gtk_minver dnl
   libgnomeui-2.0 >= libgnomeui_minver])
AC_SUBST(FONT_VIEW_CFLAGS)
AC_SUBST(FONT_VIEW_LIBS)
if test -n "$export_dynamic"; then
  FONT_VIEW_LIBS=`echo $FONT_VIEW_LIBS | sed -e "s/$export_dynamic//"`
fi

PKG_CHECK_MODULES(CONTEXT_MENU,
  [gnome-vfs-2.0 >= gnomevfs_minver dnl
   libbonobo-2.0 >= libbonobo_minver])
AC_SUBST(CONTEXT_MENU_CFLAGS)
AC_SUBST(CONTEXT_MENU_LIBS)
if test -n "$export_dynamic"; then
  CONTEXT_MENU_LIBS=`echo $CONTEXT_MENU_LIBS | sed -e "s/$export_dynamic//"`
fi

AC_PATH_PROG(GCONFTOOL, gconftool-2, no)
if test "x$GCONFTOOL" = xno; then
  AC_MSG_ERROR([gconftool-2 executable not found.  Should be installed with GConf])
fi
AM_GCONF_SOURCE_2

dnl Checks for i18n
ALL_LINGUAS="am be ca cs da de el es fa fr ga he id it ja ko lv nl mn ms no pl pt pt_BR ru sr sr@Latn sv th uk vi zh_TW"
GETTEXT_PACKAGE=fontilus
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE",
                   [domain used for gettext() calls])

AM_GLIB_GNU_GETTEXT
GLIB_DEFINE_LOCALEDIR(FONTILUS_LOCALEDIR)
AC_PROG_INTLTOOL

dnl add debugging options ...
changequote(,)dnl
if test "x$GCC" = xyes; then
  case " $CFLAGS " in
  *[\	\ ]-Wall[\	\ ]*) ;;
  *) CFLAGS="$CFLAGS -Wall" ;;
  esac

  case " $CFLAGS " in
  *[\	\ ]-std=c9x[\	\ ]*) ;;
  *) CFLAGS="$CFLAGS -std=c9x" ;;
  esac
fi
changequote([,])dnl

AC_OUTPUT([
fontilus.spec
Makefile
src/Makefile
po/Makefile.in
])
