Fontilus
--------

Fontilus is a set of extensions for Nautilus to help manage fonts on
your system.  It consists of a number of components:

  - libfont-method: a gnome-vfs method implementing the fonts:/// URI
    scheme.

  - gnome-thumbnail-font: a program to render a small preview of a
    font.  Uses a custom stream implementation, so can render fonts
    from arbitrary gnome-vfs URIs (provided the file can be openned in
    random access mode).  For example, it can render files in
    fonts:///

  - gnome-font-viewer: a simple program to display info about a font
    and render some of the glyphs.

The fonts:/// URI scheme looks up fonts using fontconfig (so
fontconfig is a requirement).  If you write a file to fonts:///, it
will be placed in ~/.fonts, so that it is available for use (ie. DnD
font installation).  Similarly, you can delete a font in fonts:/// to
remove it (provided you have permission to delete the font file).

If you have a sufficiently recent libgnomeui, the font thumbnailing
support should be enabled automatically.  If you have an older
libgnomeui-2.1, you will need to create a ~/.gnome2/thumbnailrc file
with the following entries:

    application/x-font-ttf:   gnome-thumbnail-font %u %o
    application/x-font-type1: gnome-thumbnail-font %u %o
    application/x-font-pcf:   gnome-thumbnail-font %u %o
    application/x-font-otf:   gnome-thumbnail-font %u %o

The font viewer should execute when ever you double click on a font in
the filesystem, or under the special fonts:/// URI scheme.
